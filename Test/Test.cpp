/*��������� � ������� ���� 5 �������: �������, �������������, ��������������, ����������� � ����������� ����� ������.
  � ������� ������ ���� ����� print, ������� ������� �� ����� ������ � ����������� �� ������, ����� ���� ������� � ������, ������� ��������� � ��������
  ������� ���� �� ���, ������ ����� ����� ���� ���-�� �� ������� ������� ���� ��� ������� ��� ��� ������������ ������, ��� � ������� ���� ������ � ����, 
  �� � � �� ����������� ������������ ������ � ������������ ����������� � �������� �� ��������.
  ������� ��� ������ ���������. ����� ������ �� ������, � �� ������������ =(*/




#include <iostream>
#include <math.h>
#include <string>

class square;
class rectangle;
class parallelogram;
class triangle;

//����� ������ 
class figure  {

//���� ������
protected:
	int perim; //��������
	int sq; //�������
	int a; //������ �������
	int b; //������ �������

public:
	// ����������� �� ����������
	figure() {
		a = 0;
		b = 0;
		sq = 0;
		perim = 0;
	}

	//������ �� ������ ������� (�������)
	void setA(int a);

	//������ �� ������ ������� (�������)
	void setB(int b);

	//������ �� ������ �������
	int getA() {
		return a;
	}

	//������ �� ������ �������
	int getB() {
		return b;
	}

	//������ �� �������
	int getSq() {
		return sq;
	}

	//������ �� ��������
	int getPerim() {
		return perim;
	}

	//������������� ������ � �������
	/*friend class square;
	friend class rectangle;
	friend class parallelogram;
	friend class triangle;*/
	friend  void someFriendFunc(square& value);
};

//����� �������
class square : public figure {

public:

	//������ ��������� ����� (���������� ��� � square::nested)
	class nested {
	public:
		void printNes() {
			std::cout << "nested class" << std::endl;
		}
	};

	//����������� �� ���������
	square() : figure() {
		
	}

	// ����������� 
	square(int a) {
		this->a = a; // ���������� ������� ������� ����
		sq = a * a; // ���������� ������� ���������
		perim = 4 * a; // ���������� ��������� ���������
	}

	//����� ������ �� ������ ���������
	void print() {
		for (int i = 1; i <= a; i++)
		{
			std::cout << std::endl;

			for (int j = 1; j <= a; j++)
			{
				std::cout << "* ";
			}
		}
		std::cout << std::endl;
	}
};

//����� �������������
class rectangle : public figure {

public:

	//����������� �� ���������
	rectangle() : figure() {

	}

	//�����������
	rectangle(int a,int b) {
		this->a = a; // ���������� ������ ������� ������� ����
		this->b = b; // ���������� ������ ������� ������� ����
		sq = a * b; // ���������� ������� ���������
		perim = 2 * a + 2 * b; // ���������� ��������� ���������
	}
	//����� ������ �� ������ ��������������
	void print() {
		for (int i = 1; i <= a; i++)
		{
			std::cout << std::endl;

			for (int j = 1; j <= b; j++)
			{
				std::cout << "* ";
			}
		}
		std::cout << std::endl;
	}
};

//����� ��������������
class parallelogram : public figure {
	
public:

	//����������� �� ���������
	parallelogram() : figure() {

	}

	//�����������
	parallelogram(int a, int b) {
		this->b = b; //���������� ������ ������� ������� � ����
		this->a = a; //���������� ������ ������� ������� � ����
		sq = a * b; // ���-�� ����� �������
		perim = 2 * a + 2 * b; // ���-�� ����� ���������
	}
	

	//����� �� ������ ���������������
	void print() {
		for (int i = 0; i < a; i++)
		{
			for (int j = 0; j < a - i - 1; j++)
				std::cout << " ";

			for (int j = 0; j < b; j++)
				std::cout << "*";

			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
};

//����� ������������� ����������
class triangle : public figure {
	
public:

	//����������� �� ���������
	triangle() : figure() {

	}

	//�����������
	triangle(int a) {
		this->a = a; //���������� ������ ������� � ����
		b = sqrt(pow(a, 2) + pow(a, 2)); //���-�� ����� ����������
		sq = a * a / 2; // ���-�� ����� �������
		perim = 2.0 * a + b; // ���-�� ����� ���������
	}
	
	//������ �� ���������� ������������
	double  getB() {
		return b;
	}
	
	//������ �� ���-�� ����� ���������
	double getSomethingLikePerim() {
		return perim;
	}

	//����� �� ������ ������������
	void print() {
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < a; j++)
				if (j <= i)
					std::cout << "*";
				else
					std::cout << " ";
			std::cout << std::endl;
		}
	}
};

//������ ���������� ����� ������
void figure::setA(int a)
{
	this->a = a;
}

//������ ���������� ����� ������
void figure::setB(int b)
{
	this->b = b;
}

//friend ������� � ������ figure ������� ����� ������ � ��� private ����� (����� ���� friend ������)
void someFriendFunc(square& value) {
	value.a = 10; //������ private ���� ������ square �� 10
}

int main() {
	/*square k;
	std::cout << k.getSq();
	square a(10);
	a.print();*/

	//square k(4);
	//
	//
	//std::cout << k.getPerim();
	//rectangle r(10,15);
	//std::cout << r.getPerim();

	//triangle tt(15);
	//std::cout<<tt.getPerim();

	//tt.print();
	




	return 0;
}

